﻿using System;

namespace Treci_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDo = new ToDoItem("naslov", "text", new DateTime(2018, 1, 1));
            careTaker.AddPreviousState(toDo.StoreState());
            toDo.ChangeTask("novi zadatak");
            toDo.Rename("novi naslov");
            careTaker.AddPreviousState(toDo.StoreState());

            Console.WriteLine(toDo.ToString());

            toDo.RestoreState(careTaker.Undo());
            Console.WriteLine(toDo.ToString());
            toDo.RestoreState(careTaker.Undo());
            Console.WriteLine(toDo.ToString());
            toDo.RestoreState(careTaker.Redo());
            Console.WriteLine(toDo.ToString());
        }
    }
}
