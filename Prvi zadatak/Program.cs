﻿using System;

namespace Prvi_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("dobar dan!", "danas je srijeda"));
            notebook.AddNote(new Note("Ovo je rppoon", "ja se zovem jelena"));
            notebook.AddNote(new Note("danas je suncano i toplo", "svibanj"));

            Iterator iterator = new Iterator(notebook);
            iterator.Current.Show();
            iterator.Next().Show();

            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
