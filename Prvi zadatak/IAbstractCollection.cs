﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prvi_zadatak
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
