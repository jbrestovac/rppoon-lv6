﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Šesti_zadatak
{
    class StringDigitChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            if (stringToCheck.Any(char.IsDigit))
            {
                return true;
            }
            return false;
        }
    }
}
