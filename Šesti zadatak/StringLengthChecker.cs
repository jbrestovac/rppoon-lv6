﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Šesti_zadatak
{
    class StringLengthChecker: StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {


            if (stringToCheck.Any(char.IsDigit) && stringToCheck.Any(char.IsLower) && stringToCheck.Any(char.IsUpper ) && stringToCheck.Length>=8)
            {
                return true;
            }
            return false;
        }
    }
}
