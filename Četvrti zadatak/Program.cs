﻿using System;

namespace Četvrti_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            BankCareTaker bankCare = new BankCareTaker();
            BankAccount bankAccount= new BankAccount("Jelena", "Slavonski Brod", 21000);
            BankMemento memento = new BankMemento(bankAccount);
            bankCare.PreviousState = memento;
            Console.WriteLine(bankCare.PreviousState.Balance.ToString());

            bankAccount.UpdateBalance(50000);
            memento.AddPreviousState(bankAccount);
            bankCare.PreviousState = memento;
            Console.WriteLine(bankCare.PreviousState.Balance.ToString());
        }
    }
}
