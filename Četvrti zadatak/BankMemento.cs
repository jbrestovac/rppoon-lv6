﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Četvrti_zadatak
{
    class BankMemento
    {
        public string OwnerName { get; private set; }
        public string OwnerAdress { get; private set; }
        public decimal Balance { get; private set; }

        public BankMemento(string ownerName, string ownerAdress, decimal balance)
        {
            this.OwnerName = ownerName;
            this.OwnerAdress = ownerAdress;
            this.Balance = balance;

        }
        public BankMemento(BankAccount bankAcccount)
        {
            this.OwnerName = bankAcccount.OwnerName;
            this.OwnerAdress = bankAcccount.OwnerAddress;
            this.Balance = bankAcccount.Balance;
        }

        public void AddPreviousState(BankAccount bankAccount)
        {
            Balance = bankAccount.Balance;
        }
    }
}
