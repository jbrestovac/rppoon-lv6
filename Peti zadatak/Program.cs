﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peti_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger =
           new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            logger.Log("Jelena", MessageType.INFO);
            fileLogger.Log("Neispravno", MessageType.WARNING);
            fileLogger.Log("Nema informacija", MessageType.INFO);

        }
    }
}
