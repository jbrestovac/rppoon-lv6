﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drugi_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("biljeznica", 3.00));
            box.AddProduct(new Product("olovka", 2.00));
            box.AddProduct(new Product("gumica za brisanje", 1.50));
            Iterator iterator = new Iterator(box);
            iterator.Current.Show();
            iterator.Next();
            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();


            }
        }
    }
}
